# CarbonCollins - Cloud CI/CD Template

[[_TOC_]]

## Description

A generic gitlab CI template for [CarbonCollins - Cloud](https://gitlab.com/carboncollins-cloud) which was heavily inspired by the work done at the
[Internet Archive - Nomad](https://gitlab.com/internetarchive/nomad).

## Basic Usage

The absolute basic usage involves adding two lines to a `.gitlab-ci.yml` file in a repo you want to
deploy:

```yaml
include:
- remote: 'https://gitlab.com/carboncollins-cloud/cicd/gitlab-pipeline-template/-/raw/main/full.gitlab-ci.yml'
```

This will run a 4 stage deployment where the following will occur:

1. `nomad-render` - A Levant variable file will be populated with some defaults and basic deployment info. These will rendered into your custom `job.template.nomad` file which should be placed in the root of your repo. 
2. `nomad-validate` - Will trigger a static validation of the job configurations.
3. `nomad-plan` - Will trigger a plan within the nomad cluster to ensure it can be deployed. If it passes a check index will be saved (should help to prevent colliding parallel deployments)
4. `nomad-deploy` - Deploys the rendered job file to the nomad cluster using the check index. 

## Alternate Usage

A slightly modified version can be used within the [CarbonCollins - Cloud](https://gitlab.com/carboncollins-cloud) group
and all of its sub-projects where a set of group level variables are defined allowing a slightly more dynamic/future
resistant config:

```yaml
include:
  - project: '$CICD_TEMPLATE_PROJECT'
    ref: '$CICD_TEMPLATE_REF'
    file: '$CICD_TEMPLATE_FILE'
```

This allows group level customisaton if changes are needed.

## Static jobs

By default a set of GitLab built in jobs are added:

* [SAST](https://docs.gitlab.com/ee/user/application_security/sast/)
* [Dependency Scanning](https://docs.gitlab.com/ee/user/application_security/dependency_scanning/)
* [Secret Detection](https://docs.gitlab.com/ee/user/application_security/secret_detection/)
* [SAST IaC](https://docs.gitlab.com/ee/user/application_security/iac_scanning/)

These perform various security related checks on all pipelines.

## Customised Jobs

The templates can be customised by adding additional info to the `.gitlab-ci.yml` file.

### Dynamic variables

You can provide the render stage extra variables to be used by Levant when rending the job file.
This can be done by adding a `before_script` in the render stage like so:

```yaml
include:
- remote: 'https://gitlab.com/carboncollins-cloud/cicd/gitlab-pipeline-template/-/raw/main/full.gitlab-ci.yml'

nomad-render:
  before_script:
  - 'echo "someVariable: theValueHere" >> "$LEVANT_VAR_FILE"'

```

This variable can then be used within the job file like:

```hcl
job "blah" {
  meta {
    someValue = "[[ .someVariable ]]"
  }
}
```

### Test / Build stages

There are two predefined stages are present within the template (not used by default) which allow for testing and building of applications before deployment (for instance building a docker image).

```yaml
include:
- remote: 'https://gitlab.com/carboncollins-cloud/cicd/gitlab-pipeline-template/-/raw/main/full.gitlab-ci.yml'

my-test-stage:
  stage: test
  ...other gitlab-ci options here

my-build-stage:
  stage: build
  ...other gitlab-ci options here
```

The full stage order is:

1. `test`
2. `build`
3. `publish`
4. `validate`
5. `plan`
6. `deploy`
