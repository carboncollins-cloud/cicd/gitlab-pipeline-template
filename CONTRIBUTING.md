# Contributing

This repository is a part of the [CarbonCollins - Cloud](https://gitlab.com/carboncollins-cloud) project which
is maintained by [Steven](https://gitlab.com/CarbonCollins) as a personal/hobby project.

Any work done here is made publically available on the chance that it helps others with their own projects and to provide inspiration in that regard. 

[[_TOC_]]

## How Can I Contribute?

### Reporting Issues

If you notice any issues/bugs/problem regarding any code or configs within the repository feel free to [raise an issue](#raising-an-issue) with the `Bug` label.

### Suggesting Enhancements

If you have any sugestions regarding any of the code or configurations within this repository feel free to [create an issue](#raising-an-issue) with your suggestion inside and add the `Suggestion` label.

### General Questions

If you have any questions regarding any of the code or configurations within this repository feel free to [create an issue](#raising-an-issue) with your question inside and add the `Question` label.

## Raising Issues

This can be done in the repositiory by [creating a new issue](https://gitlab.com/carboncollins-cloud/cicd/gitlab-pipeline-template/-/issues/new).
