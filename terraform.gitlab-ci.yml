.terraform_variables:
  variables:
    TF_VAR_FILE: "$CI_PROJECT_DIR/terraform.var.yml"
    TF_ROOT: ${CI_PROJECT_DIR}/tf/environments/production

.terraform_rules:
  rules:
    - if: '$SKIP_TERRAFORM_DEPLOY == "true"'
      when: never
    - exists:
        - tf/environments/production/main.tf
      when: on_success

# Terraform
terraform-validate:
  extends: .terraform_variables
  stage: validate
  tags:
    - terraform
  cache:
    key: tf-production-no-backend
    paths:
      - ${TF_ROOT}/.terraform
  rules:
    - !reference [.terraform_rules, rules]
  script:
    - touch ${TF_VAR_FILE}
    - cd ${TF_ROOT}
    - terraform init -input=false -backend=false
    - terraform validate

terraform-apply:
  extends: .terraform_variables
  stage: deploy
  tags:
    - terraform
  environment:
    name: production
  id_tokens:
    C3_VAULT_ID_TOKEN:
      aud: https://vault.cloud.carboncollins.se
  cache:
    key: tf-production
    paths:
      - ${TF_ROOT}/.terraform
  rules:
    - !reference [.main_rules, rules]
    - !reference [.terraform_rules, rules]
  needs:
    - job: terraform-validate
  allow_failure: false
  before_script:
    - 'echo "Vault URL: ${VAULT_ADDR}"'
    - vault -version

    - 'echo "Consul URL: ${CONSUL_HTTP_ADDR}"'
    - consul -version

    - 'echo "Nomad URL: ${NOMAD_ADDR}"'
    - nomad -version

    - 'echo "Logging into Vault using pipeline JWT token with role: gitlab-repository-${CI_PROJECT_ID}"'
    - export C3_VAULT_ACCESS_TOKEN=$(vault write -field=token auth/gitlab-c3-pipeline/login role=gitlab-repository-${CI_PROJECT_ID} jwt=${C3_VAULT_ID_TOKEN})

    - 'echo "Obtaining consul deploy token with role: gitlab-repository-${CI_PROJECT_ID}"'
    - export C3_CONSUL_JOB_TOKEN_JSON=$(VAULT_TOKEN=${C3_VAULT_ACCESS_TOKEN} vault read -format="json" "consul/creds/gitlab-repository-${CI_PROJECT_ID}")
    - export C3_CONSUL_JOB_TOKEN_SECRET_ID=$(jq -r '.data.token' <<< "${C3_CONSUL_JOB_TOKEN_JSON}")
    - export C3_CONSUL_JOB_TOKEN_ACCESSOR_ID=$(jq -r '.data.accessor' <<< "${C3_CONSUL_JOB_TOKEN_JSON}")
    - 'echo "Obtained consul job token with accessorId: ${C3_CONSUL_JOB_TOKEN_ACCESSOR_ID}"'

    - 'echo "Obtaining job nomad token with role: gitlab-repository-${CI_PROJECT_ID}"'
    - export C3_NOMAD_JOB_TOKEN_JSON=$(VAULT_TOKEN=${C3_VAULT_ACCESS_TOKEN} vault read -format="json" "nomad/creds/gitlab-repository-${CI_PROJECT_ID}")
    - export C3_NOMAD_JOB_TOKEN_SECRET_ID=$(jq -r '.data.secret_id' <<< "${C3_NOMAD_JOB_TOKEN_JSON}")
    - export C3_NOMAD_JOB_TOKEN_ACCESSOR_ID=$(jq -r '.data.accessor_id' <<< "${C3_NOMAD_JOB_TOKEN_JSON}")
    - 'echo "Obtained job token with accessorId: ${C3_NOMAD_JOB_TOKEN_ACCESSOR_ID}"'

    # For now...
    - export TERRAFORM_VAULT_SKIP_CHILD_TOKEN="true"

    - export CONSUL_HTTP_TOKEN="${C3_CONSUL_JOB_TOKEN_SECRET_ID}"
    - export VAULT_TOKEN="${C3_VAULT_ACCESS_TOKEN}"
    - export NOMAD_TOKEN="${C3_NOMAD_JOB_TOKEN_SECRET_ID}"

  script:
    - touch ${TF_VAR_FILE}
    - cd ${TF_ROOT}

    - terraform init -input=false
    - terraform plan -input=false -var-file "$TF_VAR_FILE" -out ./infrastructure.plan
    - terraform apply -input=false -auto-approve ./infrastructure.plan
