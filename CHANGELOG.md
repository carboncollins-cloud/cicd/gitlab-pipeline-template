# Changelog
All notable changes to this project will be documented in this file.

The format is based on [Keep a Changelog](https://keepachangelog.com/en/1.0.0/)

## [Unreleased]

### Changed
- Nomad deployment role name moved to environment variable provided by group
- seperate deploy and validate tokens
- locked JWT job token to v1 in prep for runner 15 to be released

### Removed
- nomad-plan job

## [v1.0.0] - 2022-03-17

### Changed
- Updated all links to point to new repository location
- Moved repository to [CarbonCollins - Cloud](https://gitlab.com/carboncollins-cloud) GitLab Group

## [v0.x.x]

### Added
- Multi-arch docker container build stages
- Nomad job file deployment stages
- Custom/per-pipeline Levant variables for Nomad deployments
- Terraform deployment stages
- GitLab security stages
- Basic Contributing, License, Changelog, and Readme files.
